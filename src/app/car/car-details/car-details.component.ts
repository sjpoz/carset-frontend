import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Car } from '../car.model';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  @Input()
  car: Car;

  @Output()
  carChange = new EventEmitter<Car>()

  constructor() {

  }

  ngOnInit(): void {
  }

  notifyOnCarChange(event: Event){
    event.preventDefault();
    const formElement = event.target as HTMLFormElement;
    const markElement = formElement.querySelector<HTMLInputElement>('#mark');
    const modelElement = formElement.querySelector<HTMLInputElement>('#model');
    this.carChange.emit({id: this.car.id, mark: markElement.value, model: modelElement.value})
  }

}
