import { Component, OnInit } from '@angular/core';
import { Car } from '../car.model';
import { CarService } from '../car.service';
import { Observable } from 'rxjs';
import { CarDTO } from '../carDTO';

@Component({
  selector: 'app-car-overview',
  templateUrl: './car-overview.component.html',
  styleUrls: ['./car-overview.component.scss'],
  providers: [CarService]
})
export class CarOverviewComponent implements OnInit {

  cars$: Observable<Car[]>
  selectedCar: Car | null = null;
  carsFromApi$: Observable<CarDTO[]>

  constructor(private readonly cars: CarService) {
    this.cars$ = this.cars.values$;    
    //this.carsFromApi$ = this.cars.getCars();
  }



  ngOnInit(): void {
  }

  selectCar(car: Car){
    this.selectedCar = car;
  }

  isCarSelected(car: Car): boolean {
    return this.selectedCar === car;
  }

  applyCarChanges(updatedCar: Car){
    this.cars.updateCar(updatedCar);    
    this.selectedCar = updatedCar;
  }

}
