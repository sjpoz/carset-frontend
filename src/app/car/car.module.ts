import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarOverviewComponent } from './car-overview/car-overview.component';



@NgModule({
  declarations: [CarDetailsComponent, CarOverviewComponent],
  exports: [CarDetailsComponent, CarOverviewComponent],
  imports: [
    CommonModule
  ]
})
export class CarModule { }
