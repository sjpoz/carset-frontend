import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Car } from './car.model';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { CarDTO } from './carDTO';

@Injectable()
export class CarService {
    private row: number = 0
    constructor(private http: HttpClient) { }

    carsUrl = 'http://localhost:8080/cars';    
    
    getCars():Observable<CarDTO[]> {
        return this.http.get<CarDTO[]>(this.carsUrl);
    }

    private carSubject = new BehaviorSubject<Car[]>(
        [
            {
                id: 1,
                mark: 'VW',
                model: 'Up'
            },
            {
                id: 2,
                mark: 'VW',
                model: 'Passat'
            }
        ]
    )
    
    readonly values$ = this.carSubject.asObservable();

    updateCar(updatedCar: Car): void {
        const currentCars = this.carSubject.getValue();
        this.carSubject.next(currentCars.map(
            car => car.id === updatedCar.id ? updatedCar : car
        ))

    }
}