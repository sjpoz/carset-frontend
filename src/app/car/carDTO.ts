export class CarDTO {
    id: string;
    mark: string;
    model: string;
  }